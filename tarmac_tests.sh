#!/bin/bash
# For running tests in Jenkins by Tarmac
set -e
make check
make check-race
make check-format
