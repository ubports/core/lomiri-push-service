# run acceptance tests, expects properly setup GOPATH and deps
# can set extra build params like -race with BUILD_FLAGS envvar
# can set server pkg name with SERVER_PKG
set -ex
go test $BUILD_FLAGS -i gitlab.com/ubports/development/core/lomiri-push-service/server/acceptance
go build $BUILD_FLAGS -o testserver gitlab.com/ubports/development/core/lomiri-push-service/cmd/lomiri-push-server
go test $BUILD_FLAGS -server ./testserver $*
