/*
 * Copyright (C) 2022 Guido Berhoerster <guido+ubports@berhoerster.name>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef	LPS_FIND_HELPER_H
#define	LPS_FIND_HELPER_H

#include <glib.h>

#define	LPS_FIND_HELPER_ERROR	lps_find_helper_error_quark ()

typedef enum {
	/* failed to find a helper for given app */
	LPS_FIND_HELPER_ERROR_NO_HELPER
} LpsFindHelperError;

GQuark	lps_find_helper_error_quark(void);
gchar *	find_helper(const gchar *, const gchar *, GError **);

#endif /* !LPS_FIND_HELPER_H */
