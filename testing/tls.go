/*
 Copyright 2013-2014 Canonical Ltd.

 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License version 3, as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranties of
 MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package testing

import (
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
)

// key&cert generated with openssl req -x509 -noenc -newkey rsa:2048 -days 3650 -keyout testing.key -out testing.cert -subj "/O=Acme Co/CN=push-delivery/" -addext "subjectAltName = DNS:push-delivery"
var (
	TestKeyPEMBlock = []byte(`-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCmaOoAyYUUVd1m
vLUY+69Y7tumfvBsolGJ533Kg4LzylGBgvHqdDvixNdW0AukKLkE7QgJoi3Ky8lL
z+Qg/MHQrAXlpql/yM9lcJXdKoSkKt5SVBIAFDumKptvYTUJfBZ6B3vUQe8eRybu
KbosEy2FQ+Reuj3f/6u4ZLHvUskELSjepMPbIGOajCg1YAk2osXCTZfNpNtymHal
X+yCO5REKCSXZF+J/ezwtO5arm7kKt34zSiZCRe8m5uvjoZJsV4you2lal81Rzjd
pegRy3R9kRLhQHyMmFFMH1w0KiyWR511AnKgj7gFFC4tTrt6/5OlYueP8Q6Zkt+t
YBpiI9LDAgMBAAECggEABZcJoUwjscqunMhjBvtRaRng/AdQmT2copbJpXdkez8F
HOYirT8JUfVNzIdBlNqfWHiD6BrwEdtloCvUHH2RHyrgKdY+5GGBAgXVqu/GM+OS
w046qpEDbfIJdaTczLxD3waIVNmAKFK4jpCNRfTA9zSQI9R6xp8GjDOpvIHZ6c9y
q7njejcdjpbDtpCv7gAPVkJg64bYV2cM+sEbdej4PsUkeOg+0ySbHzGkSHafWm80
JWQ9Y6iDeVgVp+kwh6naPJUYgY4tTsFWqnrSLmcAJ62qoys7qgtLDjRy35WMOI/k
o/fQSsZZHJGSRqqyHMG53VvMFkYq1SjWYxRH1RxWQQKBgQDVNrJXx1LkvUx1Zav1
k2dxm1/mljaho8NcWk/ftADT0sTia6faWsegqzH+J4DyGofpm3L1hfOx9jlzFTvA
CuOkyQq0/zmXho5cXVF6oOhPBtrJwxvF9AVQqyUyDcOIIxWAmuJvuq3H2rGOCcJY
6DIgmIoylB5qNYwVLpCtOS+gwQKBgQDHzcpljahtZdh2A7Gxh6xvJqu8+OWbZl0N
hkyq1YFgXTm3fvh++edeQr+GbVSyVHky+QMgxYiA8aJn3K54s2RwvglbDUFd0EDO
kXu2f+zKkRI61XsS5L7Uf8cMb36tgeR4JsMDAQTHTSIMMo1mLwyixmrh+jkxShNJ
QUa+kliQgwKBgClsuBe/3UYd1UYV2+QZoIZ9FpPZaYHcLG+8CDrBYUvGboRDQFh3
PMPlnw/vIulm6DMmBZ7YSLGx0hWe8X3tX687dD4YPFtJRE2Dv/ngW2JHRajfpHzA
XWv/5HkmHXBMyLe03/uvPGZRAiBDmyPk8SBOIsOS7ZnbD31qD6foLW3BAoGAHs9z
GHnhsQz1di1Xp2Kein1SGk/fSDlevAkrpWRLEQsPLWac9yeeMADm+9m8J8uy7NNN
Ui5jwE6ClrWHCgHPyTrkwm+m1sqYU8JHa3sKjqoUP5JP6jfv3WDe1uISjLrs+kIv
7DSqAfm4+FyLRWiX8DncnYAydZFQrAku2ZBDq/UCgYEAjFob5qOLe523h6DknlEr
GnmVf0Q4OxDm4WpJFg6mRABbqggBxc4IR82hVDGQpZDFYo2UmVgua3WA16y2sXPO
XGlbeV9SdDTC/fQenKgTNiwudXUplQt3455Fel30f/FFYWltUhlsva6ObXKzSLtd
f1W2QjtrvQYwqVVLocAro74=
-----END PRIVATE KEY-----`)

	TestCertPEMBlock = []byte(`-----BEGIN CERTIFICATE-----
MIIDTzCCAjegAwIBAgIUNxcfSgLkSCGgs7jYHXQNqYLT1igwDQYJKoZIhvcNAQEL
BQAwKjEQMA4GA1UECgwHQWNtZSBDbzEWMBQGA1UEAwwNcHVzaC1kZWxpdmVyeTAe
Fw0yNDExMjgxMzAzMjNaFw0zNDExMjYxMzAzMjNaMCoxEDAOBgNVBAoMB0FjbWUg
Q28xFjAUBgNVBAMMDXB1c2gtZGVsaXZlcnkwggEiMA0GCSqGSIb3DQEBAQUAA4IB
DwAwggEKAoIBAQCmaOoAyYUUVd1mvLUY+69Y7tumfvBsolGJ533Kg4LzylGBgvHq
dDvixNdW0AukKLkE7QgJoi3Ky8lLz+Qg/MHQrAXlpql/yM9lcJXdKoSkKt5SVBIA
FDumKptvYTUJfBZ6B3vUQe8eRybuKbosEy2FQ+Reuj3f/6u4ZLHvUskELSjepMPb
IGOajCg1YAk2osXCTZfNpNtymHalX+yCO5REKCSXZF+J/ezwtO5arm7kKt34zSiZ
CRe8m5uvjoZJsV4you2lal81RzjdpegRy3R9kRLhQHyMmFFMH1w0KiyWR511AnKg
j7gFFC4tTrt6/5OlYueP8Q6Zkt+tYBpiI9LDAgMBAAGjbTBrMB0GA1UdDgQWBBTV
mA2/8D59Sr6RH2L+cbp2O/EdijAfBgNVHSMEGDAWgBTVmA2/8D59Sr6RH2L+cbp2
O/EdijAPBgNVHRMBAf8EBTADAQH/MBgGA1UdEQQRMA+CDXB1c2gtZGVsaXZlcnkw
DQYJKoZIhvcNAQELBQADggEBAGj5Qk7xOnwJiB6mJ3VD9vjE5wHooS64qae7qJCA
7sErAGJ0ipP/f/SxOJshDs1HcuTrB7Ait4myyTgOvfTiInFEj3mZ+//UUoS6I7vF
EYW0WqYRMua4kOk3w7+++yeGCoYoZMM2EY/0KyxdAgxtLjo7Hhom91ngmStNFiEq
bM7JebAn4LnsSfUCe8sY4th8y4RKAB1DmsqgBMLYB6ChSaJBva5tqp85wOcgLEa3
sqrkv+zeD3JeHSmOfUzv3rlXMje0OlzdudSUCfKkuEDVESUYXr8sPSxylmyBL1KU
X42DVoAR0wmTylorLeLOxLpI8dDODxCleaOgYC/g9cLktC4=
-----END CERTIFICATE-----`)

	// key&cert generated with openssl req -x509 -noenc -newkey rsa:2048 -sha512 -days 3650 -keyout testing512.key -out testing512.cert -subj "/O=Acme Co/CN=push-delivery/" -addext "subjectAltName = DNS:push-delivery"
	TestKeyPEMBlock512 = []byte(`-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDUwvL9Q7Vb1XTl
6p0ATK880qwCPoxdAohLqPyFgonxYcxKLZzaH/ZTdeSmYthNc9y46b6m8jtLm7Ac
i3SIgtXuLCFna2E000tabkJswyCrvjpQoP0z0OaInXUo528ZCakPVsgO92WO8yKC
lezYHgUKQpOx/afwj3Zur8jBbXDf9VaxYxDr2e0ox+4n4wL+yiBZ4DcAJ0aDNhEB
P8+0FdAqD664MvTMwjqfN6xbLbxZIw8uBXPPUzfW4WnZVls0xp6JFxAhA+UfQJs2
lwKe6W8R87/+BgiQ861A1Juc/M1103gQRx9Xnn1CDVujk5FnDz3GMpo1t+cqWYt4
NHYhEmJxAgMBAAECggEABFmw86mFTURH3ysidxxPoXoKdcMfEN2hmTrHy2LmzJUZ
Yvi8NNq+M/OXSikqKdRD3T08o8fUcKMgG9yknyVzYnWLyyREkQVTLJKlqvkRmL1m
I8T/mbuB4+V5/QJVcNY+xb86eObesF3LlPlj5evgN8GyfitU+kveqgb6tIIhvUHg
BtTB7gsh8WNuFCkUL+33FoRoesg4nsk09e58+BMEm4lLkflAiAChs2Qjkk6CXSoJ
SGdIxBApSc+kvUYiB6/r4esXqEXqZYqtbOWSmzERz+yEAvTy1dC//uS5Gx62ZPM8
L3mHTG0hOqmBZDApKMvcjfnL/f/1lpk+0iVnKalpAQKBgQD9D3cxHmBGSIwpyYk5
PTOVA3wP9jxm4kqpafEZS+gIitNJ9bD3p1vx34Md4PyskP2F0TZiCyThepgXGPHw
MQpM8CwKi7wWGouRsunGV4KD3IAKA5vNIthZ9ISJF8orVt1a9ETDGC2aj511Alh+
OKX82mX2z9vB/w4ljkhn4gh38QKBgQDXO6U4OFFSRgaVvuolpO9ZoYSbFx/dnQU2
Ih91FWfgZiA8HNjtL35VCN84l4RAFufROHeCabK/Z8D5vkJvRIn1b5Ac2AroD4K2
cjBmIzbfLLJ0n9CAZHeFk9zgRGwsTGt5JF/CClR2cxhCc18ZVaZTsqbDUwUhTCc0
IxRcj08SgQKBgQDsQIQGV1WKSDCS0pRJ29lMRPSN8lRxuNH7V+y+sR7u2qxnX/jX
PGeaaBEA1AsREEuforIi1kSYPNvAtIkqtJ3pNk/niI0QFbg3TdDyWsB+Wl0u/C94
VA+n65/QXWVhELeXMnT/rpLkuS9HDemP7VMJvNt0kJmZg4SxiyQN1/gQAQKBgGWs
gHUZbZkPCWdN47BaxNKzkT/uYZg/58lVv0SBEywXMl0TR4psKCKlaUWDle7sox/b
q+EIa28t5SRVM276kq1FwW9Ow4Aya44iTSjCs9YjeD3NjSJJ04hffDTZPbyDcTWt
8Cpq1NGLt0cU1zTRmBvrxgl5O3AqWE6xz5mV/UWBAoGBALTQ3Qq+Dt2DyzgSuZrD
w1OarNgdRPN9Qu+CT8XD9lbi9Qvt5PPP/KFXHQQ/o7+CX3cUyPn0zpe/xgNQR9zB
ejbyt2MmmEd1c7YeSFRo6zLEsrA06Q7jOBzogRTk6+9uIVAk1akU40rhu5UNydoq
PRyYRRO/4Z5YJFR1/U+KBv68
-----END PRIVATE KEY-----`)

	TestCertPEMBlock512 = []byte(`-----BEGIN CERTIFICATE-----
MIIDTzCCAjegAwIBAgIUVEdnmXJ4CdSUbMPmpmywD1dFCgEwDQYJKoZIhvcNAQEN
BQAwKjEQMA4GA1UECgwHQWNtZSBDbzEWMBQGA1UEAwwNcHVzaC1kZWxpdmVyeTAe
Fw0yNDExMjgxMjM3NTVaFw0zNDExMjYxMjM3NTVaMCoxEDAOBgNVBAoMB0FjbWUg
Q28xFjAUBgNVBAMMDXB1c2gtZGVsaXZlcnkwggEiMA0GCSqGSIb3DQEBAQUAA4IB
DwAwggEKAoIBAQDUwvL9Q7Vb1XTl6p0ATK880qwCPoxdAohLqPyFgonxYcxKLZza
H/ZTdeSmYthNc9y46b6m8jtLm7Aci3SIgtXuLCFna2E000tabkJswyCrvjpQoP0z
0OaInXUo528ZCakPVsgO92WO8yKClezYHgUKQpOx/afwj3Zur8jBbXDf9VaxYxDr
2e0ox+4n4wL+yiBZ4DcAJ0aDNhEBP8+0FdAqD664MvTMwjqfN6xbLbxZIw8uBXPP
UzfW4WnZVls0xp6JFxAhA+UfQJs2lwKe6W8R87/+BgiQ861A1Juc/M1103gQRx9X
nn1CDVujk5FnDz3GMpo1t+cqWYt4NHYhEmJxAgMBAAGjbTBrMB0GA1UdDgQWBBQl
5W79Mydc4Knsfh3GJlcupudT1DAfBgNVHSMEGDAWgBQl5W79Mydc4Knsfh3GJlcu
pudT1DAPBgNVHRMBAf8EBTADAQH/MBgGA1UdEQQRMA+CDXB1c2gtZGVsaXZlcnkw
DQYJKoZIhvcNAQENBQADggEBAAZZiUgMrjTt7Bcvv1SOMQI+4VAA/8bSAqjrqh4N
Ac10hFCgFCKrYu6TyDwZUyQJjurs/Olr8Ki9s5ev52FxZZVXaFDfrbW31OdDIhyc
i3FnZElCq9gXsY/FyKUzSFgzexjT/TJgyHajMeBxETPxpccsKBsCEnlcixr/Q7Sw
y4jpk1tHvLd0q/r9BHEPaylXcK9A3LYQmTB8eKK+O3lUg0vjidLVESuQsrW3VOos
/IkzdkcDQS6vt7trW3LIh3oJavU2/DISkmkGmORSRuSbnyQM9+MyhO69Z2W5JAVh
96vQgRFfEkHIdMT+I6rNOya0bfApkCWuBiNtSisMZr/bpQM=
-----END CERTIFICATE-----`)

	// key&cert, same as server/acceptance/ssl/testing.*
	TestKeyPEMBlockAcceptance []byte

	TestCertPEMBlockAcceptance []byte
)

// test tls server & client configs
var (
	TestTLSServerConfigs                     = map[string]*tls.Config{}
	TestTLSClientConfigs                     = map[string]*tls.Config{}
	TestTLSServerConfig, TestTLSClientConfig *tls.Config
)

func init() {
	var err error
	TestKeyPEMBlockAcceptance, err = ioutil.ReadFile(SourceRelative("../server/acceptance/ssl/testing.key"))
	if err != nil {
		panic(err)
	}

	TestCertPEMBlockAcceptance, err = ioutil.ReadFile(SourceRelative("../server/acceptance/ssl/testing.cert"))
	if err != nil {
		panic(err)
	}

	for _, cfgBits := range []struct {
		label string
		key   []byte
		cert  []byte
	}{
		{"sha1", TestKeyPEMBlock, TestCertPEMBlock},
		{"sha512", TestKeyPEMBlock512, TestCertPEMBlock512},
		{"acceptance", TestKeyPEMBlockAcceptance, TestCertPEMBlockAcceptance},
	} {
		cert, err := tls.X509KeyPair(cfgBits.cert, cfgBits.key)
		if err != nil {
			panic(err)
		}
		tlsServerConfig := &tls.Config{
			Certificates: []tls.Certificate{cert},
		}
		cp := x509.NewCertPool()
		ok := cp.AppendCertsFromPEM(cfgBits.cert)
		if !ok {
			panic("failed to parse test cert")
		}
		tlsClientConfig := &tls.Config{
			RootCAs:    cp,
			ServerName: "push-delivery",
		}
		TestTLSClientConfigs[cfgBits.label] = tlsClientConfig
		TestTLSServerConfigs[cfgBits.label] = tlsServerConfig
	}
	TestTLSClientConfig = TestTLSClientConfigs["sha1"]
	TestTLSServerConfig = TestTLSServerConfigs["sha1"]
}
