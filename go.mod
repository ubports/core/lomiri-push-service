module gitlab.com/ubports/development/core/lomiri-push-service

go 1.22

require (
	github.com/adrg/xdg v0.5.0
	github.com/godbus/dbus/v5 v5.1.0
	github.com/google/uuid v1.6.0
	github.com/mattn/go-sqlite3 v1.14.23
	golang.org/x/net v0.29.0
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c
)

require (
	github.com/kr/pretty v0.2.1 // indirect
	github.com/kr/text v0.1.0 // indirect
	golang.org/x/sys v0.25.0 // indirect
)
