/*
 Copyright 2013-2014 Canonical Ltd.

 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License version 3, as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranties of
 MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Package bus provides a simplified (and more testable?) interface to DBus.
package bus

// Here we define the Bus itself

import (
	"gitlab.com/ubports/development/core/lomiri-push-service/logger"
)

/*****************************************************************
 *    Bus (and its implementation)
 */

// This is the Bus itself.
type Bus interface {
	String() string
	Endpoint(Address, logger.Logger) Endpoint
}

type concreteBus int

const (
	typeSystemBus  concreteBus = iota
	typeSessionBus
)

// ensure concreteBus implements Bus
var _ Bus = new(concreteBus)

// no bus.Bus constructor, just two standard, constant, busses
var (
	SystemBus  Bus = concreteBus(typeSystemBus)
	SessionBus Bus = concreteBus(typeSessionBus)
)

/*
   public methods
*/

func (bus concreteBus) String() string {
	if bus == typeSystemBus {
		return "SystemBus"
	} else {
		return "SessionBus"
	}
}

// Endpoint returns a bus endpoint.
func (bus concreteBus) Endpoint(addr Address, log logger.Logger) Endpoint {
	return newEndpoint(bus, addr, log)
}

// Args helps build arguments for endpoint Call().
func Args(args ...interface{}) []interface{} {
	return args
}

/*****************************************************************
 *    Address
 */

// bus.Address is just a bag of configuration
type Address struct {
	Name      string
	Path      string
	Interface string
}

var BusDaemonAddress = Address{
	"org.freedesktop.DBus",
	"/org/freedesktop/DBus",
	"org.freedesktop.DBus",
}
