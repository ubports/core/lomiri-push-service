/*
 Copyright 2014 Canonical Ltd.

 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License version 3, as published
 by the Free Software Foundation.

 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranties of
 MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Package greeter retrieves information about the Lomiri Greeter
// using Lomiri's dbus interface
package greeter

import (
	"gitlab.com/ubports/development/core/lomiri-push-service/bus"
	"gitlab.com/ubports/development/core/lomiri-push-service/logger"
)

// Well known address for the Greeter API
var BusAddress bus.Address = bus.Address{
	Interface: "com.lomiri.LomiriGreeter",
	Path:      "/com/lomiri/LomiriGreeter",
	Name:      "com.lomiri.LomiriGreeter",
}

// Greeter encapsulates info needed to call out to the Greeter API
type Greeter struct {
	bus bus.Endpoint
	log logger.Logger
}

// New returns a new Greeter that'll use the provided bus.Endpoint
func New(endp bus.Endpoint, log logger.Logger) *Greeter {
	return &Greeter{endp, log}
}

// GetGreeter returns the window stack state
func (greeter *Greeter) IsActive() bool {
	result, err := greeter.bus.GetProperty("IsActive")
	if err != nil {
		greeter.log.Errorf("GetGreeter call returned %v", err)
		return false
	}
	return result.(bool)
}
